#include "objIO.hpp"

#include "OBJ_Loader.h"
#include <fstream>
#include <iostream>


using std::vector;
using std::string;
using std::cerr;
using std::endl;
using std::ofstream;
using std::ostream;

using glm::vec3;

vec3 toGlm(const objl::Vector3& v)
{
  return vec3(v.X, v.Y, v.Z);
}

ostream& operator<< (ostream& os, vec3 v)
{
  os << v.x << " " << v.y << " " << v.z;
}

namespace obj
{
  void read(vector<vec3>& verts, vector<vec3>& norms, vector<unsigned int>& indices, const string& filename)
  {
    objl::Loader objLoader = objl::Loader();
    if (!objLoader.LoadFile(filename))
    {
      cerr << "ERROR: Could not load file: " << filename << endl;
    }

    const vector<objl::Vertex>& loadedVertices = objLoader.LoadedVertices;

    verts.clear();
    norms.clear();
    for (auto v : loadedVertices)
    {
      verts.push_back(toGlm(v.Position));
      norms.push_back(toGlm(v.Normal));
    }
    indices = objLoader.LoadedIndices;
  }

  void write(const vector<vec3>& verts, const vector<vec3>& norms, const vector<unsigned int>& indices, const string& filename)
  {
    ofstream ofile;
    ofile.open(filename);
    ofile.precision(5);
    ofile << std::fixed;
    for (auto v : verts)
    {
      ofile << "v " << v <<"\n";
    }
    for (auto n : norms)
    {
      ofile << "vn " << n <<"\n";
    }
    for (unsigned int ii = 0; ii < indices.size(); ii += 3)
    {
      ofile << "f " << indices[ii]+1 << " " << indices[ii+1]+1 << " " << indices[ii+2]+1 << "\n";
    }
    ofile.close();
  }
}
