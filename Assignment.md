Modify an obj file in the following way:

* Read the filename of .obj file and a rotation angle &alpha; (in degrees) from arguments
* Find midpoint m of bounding box (bMin, bMax)
* every vertex v with v.x > m.x must be rotated around y-parallel line going through m by &alpha;* degrees
* where &alpha;* is a value that starts at 0 at m.x and linearly increases to &alpha; at bMax.x

Additionally:
* Parallel execution is outside the scope of this assignment
* Use of external libraries is allowed but it should be clear what code is external.
