
CC=g++
IMPORTS=-Ilibs/Obj-Loader-master/Source -Ilibs/glm-0.9.9-a2/glm
FLAGS=-std=c++14 $(IMPORTS)
DEPS=src/objIO.hpp

assignment: build/main.o build/objIO.o build/bendingFunction.o
	$(CC) -o bin/assignment build/main.o build/objIO.o build/bendingFunction.o $(FLAGS)

build/%.o: src/%.cpp $(DEPS)
	$(CC) -c -o $@ $< $(FLAGS)
